# Scale-stack barchart

This is a D3 implementation of the scale-stack barchart as presented at EuroVis 2013 by Hlawatsch et al.

It is merely a proof-of-principle and could still benefit from getting rid of quite a bit of hard-coded things.

![screenshot](screenshot.png "Scale-stack barchart")

Development was helped a lot by tributary.io (both the tool and the videos).

Software is released under the MIT license (see LICENSE).

Jan Aerts
