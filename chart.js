// Released under MIT license (see LICENSE)
// Copyright 2013 Jan Aerts

var svg = d3.select("svg");

var data1 = [9,195,327,71,3253];

var h = 539;

var stackHeight = 110;

var numberOfGridLines = [0,1,2,3];

var grid = svg.selectAll("line")
	.data(numberOfGridLines)
	.enter()
	.append("line")
	.attr({
      x1: 50,
      y1: function(d) { return h - stackHeight*d },
      x2: 500,
      y2: function(d) { return h - stackHeight*d },
      stroke: "#9B9B9B",
      strokeWeight: 1
	})

var bars = function(d,s) {
    var logValue = Math.log(s)/Math.log(10)
	var b = svg.selectAll("rect".concat(s))
		.data(d)
		.enter()
		.append("rect")
		.attr({
	      x: function(d,i) {
	        if ( d > 10*s ) {
				return 80 + 60*i + 10
	        } else {
	          return 80 + 60*i
	        }},
	      y: function(d) { 
	        if ( d > 10*s ) {
	          return h - logValue*stackHeight - stackHeight
	        } else if ( d > s) {
	          return h - logValue*stackHeight - (stackHeight*d)/(10*s) // or d/s???
            } else {
              return h - logValue*stackHeight - 2;
            }
	      },
	      width: function(d) {
	        if ( d > 10*s ) {
	          return 2;
			} else {
	          return 20;
	        }
	      },
	      height: function(d) {
	        if ( d > 10*s ) {
	          return stackHeight;
			} else if (d > s) {
	          return (stackHeight*d)/(10*s)
            } else {
              return 2
            }
	      },
	      fill: "#424242",
	      opacity: 0.5
	    })
		.classed(toString(s), true)
    
    return b;
}

bars(data1,1);
bars(data1,10);
bars(data1,100);
bars(data1,1000);

var labels = svg.selectAll("text")
	.data(data1)
	.enter()
	.append("text")
    .text(function(d) { return d } )
	.attr({
      x: function(d,i) { return 90 + 60*i },
      y: h + 20,
      stroke: null,
      fill: "#5C5B5B"
	})
	.style("text-anchor","middle")

svg.append("text")
	.text("0")
	.attr({
	  x: 520,
	  y: h,
      fill: "#747474"
	})
svg.append("text")
	.text("10")
	.attr({
	  x: 507,
	  y: h - stackHeight,
      fill: "#747474"
	})
svg.append("text")
	.text("100")
	.attr({
	  x: 507,
	  y: h - 2*stackHeight,
      fill: "#747474"
	})
svg.append("text")
	.text("1000")
	.attr({
	  x: 507,
	  y: h - 3*stackHeight,
      fill: "#747474"
	})
